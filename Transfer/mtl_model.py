#!/usr/bin/env python

"""mtl_model.py: Interface to multi-task transfer learning models."""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import sklearn.base as sklearnbase 
import numpy as np
import pdb
import TLBase

__author__ = "Karl-Heinz Fiebig, adapted by Vinay Jayaram"
#__copyright__ = "TODO"
#__license__ = "TODO"
__maintainer__ = "Karl-Heinz Fiebig"
__email__ = "kh.fiebig.cs@gmail.com"
__version__ = "1.0.0"

class GaussianPriorTL(sklearnbase.BaseEstimator, TLBase.TransferLearningBase):
    '''
    General class for models that rely on a Gaussian prior to update some sort of 
    regularization parameter that can be trained separately for each of the
    datasets. 
    '''
    def __init__(self, lam=1, epsilon=1e-6, n_iter=int(1e3), model=None, tol=1e-4):
        self.lam = lam
        self.epsilon = epsilon
        self.n_iter = n_iter
        self.model = model
        self.tol = tol

    def fit_multi(self, Xlist, ylist, eobj=None, initialparams=None, \
            verbose=False, normstyle='ML', l_update='ML', \
            e_update=False, **kwargs):
        if eobj is not None:
            self.model = eobj
        elif self.model is None:
            raise Exception('No model given to train')
    
        self.model.set_params(**{'lam': self.lam})
        if initialparams is not None:
            self.model = self.model.set_params(**initialparams)
        self.prior_ = self.model.set_prior(Xlist[0])
        dataset = [sklearnbase.clone(self.model) for i in range(len(Xlist))]
        for i in range(self.n_iter):
            # train objects with current prior
            prev_prior = self.prior_
            it_coef = []
            self.errors_ = []
            dataset = [d.set_params(**{'prior': self.prior_})\
                       .fit(Xlist[ind], ylist[ind]) for ind,d in enumerate(dataset)]
            for d in dataset:
                coef, train_error = d.get_computed()
                it_coef.append(coef)
                self.errors_.append(train_error)

            # update prior parameters
            self.prior_ = []
            w_eig = []
            for j in range(len(prev_prior)):
                W = [l[0] for l in it_coef]
                mu = sum(W) / len(it_coef)
                Wdm = np.concatenate([w - mu for w in W], axis=1)

                if normstyle == 'ML':
                    norm = len(it_coef) - 1
                elif normstyle == 'Trace':
                    norm = np.trace(Wdm.dot(Wdm.T))
                self.prior_.append((mu,(Wdm.dot(Wdm.T) / norm) + self.epsilon))
                weig = np.linalg.eig(Wdm.dot(Wdm.T) / norm)
                w_eig.append(weig[weig > 0].min())
            
            # update lambdas
            if l_update is not None:
                mean_err = sum(self.errors_)/len(dataset)
                for ind,d in enumerate(dataset):
                    if l_update == 'ML':
                        d.set_params(**{'lam': 2 * mean_err})
                    elif l_update == 'ML_ind':
                        d.set_params(**{'lam': 2 * self.errors_[ind]})

            # update epsilon
            if e_update:
                self.epsilon = 0.01 * min(map(lambda x:x * 0.01,w_eig))

            # check for convergence
            pdiff = []
            for d in range(len(self.prior_)):
                p = prev_prior[d][0]
                c = self.prior_[d][0]
                pdiff.append(np.sum(np.abs(p-c) > (self.tol * np.abs(p))))
            if reduce(lambda x,y: x and (y == 0), pdiff, True):
                break
            if verbose:
                print('Prior Iter', i, '; Conv: ', pdiff)

        self.lam_ = [d.get_params()['lam'] for d in dataset]
        self.__estimator_list = dataset
        return self


    def fit_new(self, X, y, initobj=None,**kwargs):
        if initobj is None:
            initobj = sklearnbase.clone(self.model)
        elif len(initobj.set_prior()) != len(self.prior_):
            raise Exception("""number of prior distributions do not match
                               between provided model and base object""")
        return initobj.set_params({'prior': self.prior_}).fit(X,y,**kwargs)

    def return_multi(self, ind=None):
        if ind is None:
            return self.__estimator_list
        else:
            return self.__estimator_list[ind]
        

    def get_computed(self):
        return self.prior_, self.errors_, self.lam_

