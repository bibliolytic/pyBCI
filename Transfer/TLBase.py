from __future__ import division
import sklearn.base 
__author__ = "Vinay Jayaram"
__maintainer__ = "Vinay Jayaram"
__email__ = "vinayjayaram13@gmail.com"
__version__ = "0.0.1"

class TransferLearningBase(object):
    """
    Base class to deal with transfer learning schemes in a consistent way. It is
    intended to provide a convenient wrapper to test different transfer learning
    approaches that require different sorts of constraints and inputs but need
    to be compared against each other.

    The class accepts datasets and related information and trains either one or
    many classifiers, each of which is an instantiation of the scikit-learn
    BaseEstimatior class. 

    """

    def __init__(self):
        """
        Initialization for the algorithm. We follow scikit-learn convention here
        and require that initialize do no processing of inputs and simply adds
        them to a dictionary that each instance stores. Any data-dependent
        altering of hyperparmameters is done in fit_multi
        """
        pass

    def fit_multi(self):
        """
        Method that implements the multitask algorithm that attempts to learn
        some information across datasets. It accepts a
        list of datasets and whatever the appropriate labelling is for the given
        scheme (e.g. trial-specific labels for supervised learning, dataset
        mutlilabels for multilinear transfer learning, etc) and trains an
        internal list of classifier objects. Depending on the algorithm this can
        correspond to a single instance or more. 
        """
        return self

    def fit_new(self):
        """
        Method that implements the transfer learning. Given whatever internal
        representation of the cross-dataset knowledge, and a new dataset and
        associated information, it returns a classifier object.
        """
        pass

    def return_multi(self,ind=None):
        """
        Convenience function that returns the trained classifier corresponding
        to input datasets ind.
        """
        pass
