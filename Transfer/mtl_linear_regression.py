#!/usr/bin/env python

import scipy
import sys
import pdb
import numpy as np
from sklearn.base import BaseEstimator
import time
import warnings

import os

# necessary imports for MatrixRegression
import autograd.numpy as autonp
from pymanopt.manifolds import Euclidean
from pymanopt import Problem
from pymanopt.solvers import SteepestDescent

__author__ = "Karl-Heinz Fiebig, adapted by Vinay Jayaram"
#__copyright__ = "TODO"
#__license__ = "TODO"
__maintainer__ = "Karl-Heinz Fiebig, VJ"
__email__ = "kh.fiebig.cs@gmail.com"
__version__ = "1.0.0"

class MatrixRegression(BaseEstimator):
    '''
    Regression using matrix-variate Gaussian prior and 
    matrix weight vector
    '''
    def __init__(self, prior=None, lam=1):
        self.prior = prior
        self.lam = lam
        
    def fit(self, X, Y, **kwargs):
        # X is trials x dimensions
        # Y is trials x dimensions
        assert(X.shape[0] == Y.shape[0])
        

    def predict(self, X):
        return self.predict_prob(X)

    def predict_prob(self,X):
        self._xdim(X)
        return self.w_.T.dot(X.T)

    def _xdim(self, X):
        if X.shape[1] != self.w_.shape[0]:
            raise ValueError('X dimensions are not labels by covariates')
        return

    def set_prior(self,X):
        if self.prior is None:
            self.prior = [(np.zeros((X.shape[1],1)), np.eye(X.shape[1]))]
        return self.prior

    def get_computed(self):
        return (self.w_,),self.training_error_def fit(self, X, y, **kwargs):

    

class BayesRegressionClassifier(BaseEstimator):
    
    def __init__(self, prior=None, lam=1):
        self.prior = prior
        self.lam = lam

    def fit(self, X, y, LDA=False, **kwargs):
        # data safety
        if X.shape[0] != y.shape[0]:
            raise ValueError('Data length %d and label length %d do not match',\
                             (X.shape[0],y.shape[0]))

        self.classes_, y = np.unique(y, return_inverse=True)
        # binary classifier for now
        if self.classes_.size > 2:
            raise ValueError("""Number of classes %d is too large for binary
                              classifier""", self.classes_.size,)
        
        # need minus/plus labels
        y_train = np.copy(y)
        y_train[y == 0] = -1

        if LDA:
            y_train[y == 0] = -np.mean(y == 0)
            y_train[y == 1] = np.mean(y == 1)
        
        # deal with mu and covariance
        self.set_prior(X) 

        covX = self.prior[0][1].dot(X.T)
        self.w_ = np.linalg.lstsq(1.0/self.lam*covX.dot(X) + np.eye(X.shape[1]),
                                  (1.0/self.lam*covX.dot(y_train))[:,None] + \
                                  self.prior[0][0])[0]

        self.training_error_ = np.sum(np.power(y_train-self.predict_prob(X),2))\
                               / y.size

        return self

    def predict(self, X):
        yprime = np.sign(self.predict_prob(X))
        yprime[yprime == -1] = 0
        return [self.classes_[int(i)] for i in yprime.squeeze()]

    def predict_prob(self,X):
        self._xdim(X)
        return self.w_.T.dot(X.T)

    def _xdim(self, X):
        if X.shape[1] != self.w_.shape[0]:
            raise ValueError('X dimensions are not labels by covariates')
        return

    def set_prior(self,X):
        if self.prior is None:
            self.prior = [(np.zeros((X.shape[1],1)), np.eye(X.shape[1]))]
        return self.prior

    def get_computed(self):
        return (self.w_,),self.training_error_


class FDBayesRegressionClassifier(BaseEstimator):
    
    def __init__(self, prior=None, lam=1, n_iter=1000, tol=0.001):
        self.prior = prior
        self.lam = lam
        self.n_iter = n_iter
        self.tol = tol

    def fit(self, X, y, LDA=False, verbose=False, v_lvl=5, **kwargs):
        # data safety
        if X.shape[0] != y.shape[0]:
            raise ValueError('Data length %d and label length %d do not match',\
                             (X.shape[0],y.shape[0]))

        self.classes_, y = np.unique(y, return_inverse=True)
        # binary classifier for now
        if self.classes_.size > 2:
            raise ValueError("""Number of classes %d is too large for binary
                              classifier""", self.classes_.size,)
        
        # need minus/plus labels
        y_train = np.copy(y)
        y_train[y == 0] = -1

        if LDA:
            y_train[y == 0] = -np.mean(y == 0)
            y_train[y == 1] = np.mean(y == 1)
        
        # deal with mu and covariance
        self.set_prior()

        # initialize holders to use for convergence testing
        self.a_ = np.ones((X.shape[1],1))/np.sqrt(X.shape[1])
        self.w_ = np.zeros((X.shape[2],1))

        for i in range(self.n_iter):
            w_prev = np.copy(self.w_) # may not need the copy 
            a_prev = np.copy(self.a_) # may not need the copy
            # optimize spectral weights
            pdb.set_trace()
            aX = np.squeeze(self.a_.T.dot(X))
            w_covX = self.prior[0][1].dot(aX.T)
            self.w_ = np.linalg.lstsq( 1.0/self.lam * w_covX.dot(aX)\
                                       + np.eye(self.w_.size), \
                                       1.0/self.lam * w_covX.dot(y_train) \
                                       + self.prior[0][0])[0]
            Xw = np.squeeze(np.tensordot(self.w_,X,[0,2]))
            a_covX = self.prior[1][1].dot(Xw.T)
            self.a_ = np.linalg.lstsq( 1.0/self.lam * a_covX.dot(Xw)\
                                       + np.eye(self.a_.size), \
                                       1.0/self.lam * a_covX.dot(y_train) \
                                       + self.prior[1][0])[0]
            diff_w = np.abs(w - w_prev)
            diff_a = np.abs(a - a_prev)
            conv_w = np.sum(diff_w < self.tol*np.abs(w_prev))
            conv_a = np.sum(diff_a < self.tol*np.abs(a_prev))
            if verbose and not (i % v_lvl):
                print('FD-CG Iter', i, \
                      '; Conv:', self.w_.size - conv_w, '/', \
                      self.a_.size - conv_a)
            if conv_w == self.w_.size and cov_a == self.a_.size:
                break

        self.training_error_ = np.sum(np.power(y_train-self.predict_prob(X),2))\
                               / y.size
        return self

    def predict(self, X, **kwargs):
        yprime = np.sign(self.predict_prob(X, **kwargs))
        yprime[yprime == -1] = 0
        return [self.classes_[i] for i in yprime]

    def predict_prob(self,X, **kwargs):
        self._xdim(X)
        yprime = np.zeros(X.shape[0])
        for ind,x in enumerate(X):
            yprime[ind] = self.a_.T.dot(x).dot(self.w_)
        return yprime

    def _xdim(self, X):
        if X.shape[1] != self.a_.shape[0] or X.shape[2] != self.w_.shape[0]:
            raise ValueError('X dimensions do not agree with weight/frequency vectors')
        return

    def set_prior(self):
        if self.prior is None:
            self.prior = []
            self.prior.append(np.zeros((X.shape[2],1)),np.eye(X.shape[2]))
            self.prior.append(np.ones((X.shape[1],1))/np.sqrt(X.shape[1]),\
                          np.eye(X.shape[1]))
        return self.prior

    def get_computed(self):
        return (self.w_, self.a_), self.training_error_
