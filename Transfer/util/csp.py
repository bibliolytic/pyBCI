import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import scipy.linalg as splinalg
import sklearn.base
def spatial_filter(x,w):
    '''

    :param x: eeg data,  Dimensions should be
                A) channels x time
                B) trials x channels x time
    :param w: spatial filter, one filter per column
    :return:
    '''
    if x.ndim==2:
        return np.dot(w.T,x)
    elif x.ndim == 3: # Already in trial format
        return  np.vstack([[np.dot(w.T,xi)] for xi in x])
    raise RuntimeError("Don't know how to use this number of dimensions")


def compute_csp_filter(x,y):
    '''
    Return the spatial filters, patterns and associated eigenvalues
    :param x:   3 dim array (trials x channels x samples per tril)
    :param y:   1-dim array with binary markers for the classes
    :return:    w, the spatial filters, one per column
                p, the spatial patterns, one per column
                d, the associated eigenvalues
    '''
    #train
    x_a = x[y==0]
    x_b = x[y==1]

    # Compute normalised covariance
    xtx_a = np.mean([np.cov(np.atleast_2d(t)) for t in x_a],axis=0)
    xtx_b = np.mean([np.cov(np.atleast_2d(t)) for t in x_b],axis=0)

    (d,u)=splinalg.eigh(a=xtx_b,b=xtx_a+xtx_b) #d contains eigenvalues, columns of u the eigenvectors
    idx_sort = np.argsort(d)
    d=d[idx_sort]
    u=u[:,idx_sort]

    # Compute filters w, patterns p and eigenvalues d
    w = u
    p = np.linalg.inv(w).T

    return w,p,d

class LSR(object,sklearn.base.BaseEstimator,sklearn.base.ClassifierMixin):
    """
    Least squares based classifier, without regularisation
    """
    def __init__(self):
        """
        Create a least squares based classifier

        :return: an untrained classifier
        """
        self.w = None

    def _add_bias(self,x):
        """

        :param x:
        :return:
        """
        return np.hstack([x,np.ones((x.shape[0],1))])

    def fit(self,x,y):
        y = np.atleast_2d(2.*y-1).T
        x = self._add_bias(x)
        self.w = np.dot(np.linalg.pinv(x),y)

    def decision_function(self,x):
        return np.dot(self._add_bias(x),self.w).flatten()

    def predict(self,x):
        return 1*(self.decision_function(x) > 0.)

    def compute_csp_features(self,x):
        return x


class ClassifierCSP(object,sklearn.base.BaseEstimator,sklearn.base.ClassifierMixin):
    def __init__(self,num_csp,clf=None):
        """

        :param num_csp: even number or auto. If an even number is proposed, the filters with the highest and the lowest eigenvalues will be returned. If auto is used, then
        :param clf: None for an automatically regularised LDA classifier. Or a specific sklearn classifier to be used here.
        :return:
        """
        self.clf = clf
        if clf is None:
            self.clf = LinearDiscriminantAnalysis(solver='eigen',shrinkage='auto')
        self.num_csp = num_csp

    def fit(self,x,y):
        # Compute csp filters and retain a subset :)
        self.w, self.p,d = compute_csp_filter(x,y)

        # select csp filters
        mrk_csp = self._select_filters(x,y,d)
        self.num_csp = np.sum(mrk_csp)
        self.w = self.w[:,mrk_csp==1]
        self.p = self.p[:,mrk_csp==1]

        # fit clf on the features
        self.clf.fit(self.compute_csp_features(x),y)


    def _select_filters(self,x,y,d):
        if self.num_csp == 'auto':
            return self._select_filters_auto(x, y, d)
        return self._select_filters_equal()


    def _select_filters_equal(self):
        assert self.num_csp % 2 == 0, 'Only even number of csp filters supported now'
        mrk_csp = np.zeros(self.w.shape[0])
        mrk_csp[:self.num_csp/2]=1
        mrk_csp[-self.num_csp/2:]=1
        return mrk_csp


    def _select_filters_auto(self, x, y, d):
        f = np.exp(self.compute_csp_features(x))
        median_plus = np.median(f[y==0],axis=0)
        median_min = np.median(f[y==1],axis=0)
        score = median_min/(median_plus+median_min)
        mrk_score = 2*(np.vstack([1-score,score]).max(axis=0)-0.5)
        idx_sorted = np.argsort(mrk_score)[::-1]
        mrk_score = mrk_score>=0.66*np.max(mrk_score)


        # The points that are allowed
        mrk_csp = np.zeros(self.w.shape[0])

        # Take filter with maximum score on each side of the eigenspectrum
        nh = self.w.shape[0]//2
        mrk_csp[idx_sorted[idx_sorted>nh][0]]=1
        mrk_csp[idx_sorted[idx_sorted<nh][0]]=1

        # If the score is above the treshold, the second and third best in each part of the eigenspectrum are also allow
        ba = idx_sorted[idx_sorted>nh][1:3]
        bb = idx_sorted[idx_sorted<nh][1:3]
        mrk_csp[ba]=mrk_score[ba]
        mrk_csp[bb]=mrk_score[bb]

        return mrk_csp

    def compute_csp_features(self,x):
        return np.log(spatial_filter(x,self.w).var(axis=2))

    def decision_function(self,x):
        return self.clf.decision_function(self.compute_csp_features(x))

    def predict(self,x):
        return self.clf.predict(self.compute_csp_features(x))
